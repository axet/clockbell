#!/bin/bash

DIR=$(readlink -f $0)
DIR=$(dirname $DIR)

COUNT=$(date +%I)

for i in $(seq 1 $COUNT); do
  aplay -q $DIR/clockbell.wav
done
