#!/bin/bash -v

DIR=$(readlink -f $0)
DIR=$(dirname $DIR)

sudo cp -f $DIR/clockbell /etc/cron.d/clockbell
sudo cp -f $DIR/clockbell.wav /usr/bin/clockbell.wav
sudo cp -f $DIR/clockbell.sh /usr/bin/clockbell.sh
